﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System;

namespace SpaceShip
{
 
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D ship_Sprite;
        Texture2D asteroid_Sprite;
        Texture2D space_Sprite;

        SpriteFont gameFont;
        SpriteFont timerFont;

        Ship player = new Ship();

        Controller gameController = new Controller();



        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
        }

        protected override void Initialize()
        {
           

            base.Initialize();
        }

        protected override void LoadContent()
        {
            
            spriteBatch = new SpriteBatch(GraphicsDevice);

            ship_Sprite = Content.Load < Texture2D>("ship");
            asteroid_Sprite = Content.Load<Texture2D>("asteroid");
            space_Sprite = Content.Load<Texture2D>("space");

            gameFont = Content.Load<SpriteFont>("spaceFont");
            timerFont = Content.Load<SpriteFont>("timerFont");
            
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            player.shipUpdtae(gameTime,gameController);
            //player.setXPostion((int)player.getPosition().X + 1);
     
            gameController.conUpdate(gameTime);
            int sum;
            for (int i = 0; i < gameController.Asteroids.Count; i++)
            {
                gameController.Asteroids[i].asteroidUpdate(gameTime);

                if(gameController.Asteroids[i].Position.X < (0 - gameController.Asteroids[i].Radius))
                {

                    gameController.Asteroids[i].OffScreen = true;
                }

                sum = gameController.Asteroids[i].Radius + 30;
                if(Vector2.Distance(gameController.Asteroids[i].Position,player.Position) < sum)
                {
                    gameController.InGame = false;
                    player.Position = Ship.defaultPosition;
                    i = gameController.Asteroids.Count;
                    gameController.Asteroids.Clear();

                }
            }

            gameController.Asteroids.RemoveAll(a => a.OffScreen);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            spriteBatch.Draw(space_Sprite, new Vector2(0, 0), Color.White);
            spriteBatch.Draw(ship_Sprite, new Vector2(player.Position.X - player.ShipWidth,player.Position.Y - player.ShipHeight), Color.White);

            if(gameController.InGame == false)
            {
                string menuMessage = "Press Enter to Begin";
                Vector2 sizeOfText =  gameFont.MeasureString(menuMessage);
                spriteBatch.DrawString(gameFont, menuMessage, new Vector2(640 - sizeOfText.X / 2, 200), Color.White);
            }

            for (int i = 0; i < gameController.Asteroids.Count; i++)
            {
                Vector2 tmpPos = gameController.Asteroids[i].Position;
                int tmpRadius = gameController.Asteroids[i].Radius;

                spriteBatch.Draw(asteroid_Sprite, new Vector2(tmpPos.X - tmpRadius, tmpPos.Y - tmpRadius), Color.White);

            }

            spriteBatch.DrawString(timerFont, "Time: " + Math.Floor(gameController.TotalTime).ToString(), new Vector2(3, 3), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
