﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpaceShip
{
    class Controller
    {
        private double timer = 2D;
        private double maxTime = 2D;
        private int nextSpeed = 240;
        private float totalTime = 0f;

        private bool inGame = false;

        private List<Asteroid> asteroids = new List<Asteroid>();

        public double Timer { get => timer; set => timer = value; }
        public List<Asteroid> Asteroids { get => asteroids; set => asteroids = value; }
        public bool InGame { get => inGame; set => inGame = value; }
        public float TotalTime { get => totalTime; set => totalTime = value; }

        public void conUpdate(GameTime gameTime)
        {
            if (InGame)
            {
                Timer -= gameTime.ElapsedGameTime.TotalSeconds;
                TotalTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else
            {

                KeyboardState kState = Keyboard.GetState();
                if (kState.IsKeyDown(Keys.Enter))
                {
                    inGame = true;
                    timer = 2D;
                    maxTime = 2D;
                    nextSpeed = 240;
                    totalTime = 0f;
                }
            }

            if(Timer <= 0)
            {
                Asteroids.Add(new Asteroid(nextSpeed));
                Timer = maxTime;
                if (maxTime > 0.5)
                {
                    maxTime -= 0.1D;
                }
                if (nextSpeed < 720)
                {
                    nextSpeed += 4;
                }
            }
        }

    }
}
