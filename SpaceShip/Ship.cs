﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpaceShip
{
    class Ship
    {
        private int shipWidth = 34;
        private int shipHeight = 50;
        private int shipSpeed = 180;
        static public Vector2 defaultPosition = new Vector2(640, 360);

        private Vector2 position = defaultPosition;

        public int ShipWidth { get => shipWidth; set => shipWidth = value; }
        public int ShipHeight { get => shipHeight; set => shipHeight = value; }
        public int ShipSpeed { get => shipSpeed; set => shipSpeed = value; }
        public Vector2 Position { get => position; set => position = value; }

        public void shipUpdtae(GameTime gameTime, Controller gameController)
        {
            KeyboardState kState = Keyboard.GetState();
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (gameController.InGame) { 
                    if (kState.IsKeyDown(Keys.Right) && position.X < 1280)
                    {
                        position.X += ShipSpeed * dt;
                    }

                    if (kState.IsKeyDown(Keys.Left) && position.X > 0)
                    {
                        position.X -= ShipSpeed * dt;
                    }

                    if (kState.IsKeyDown(Keys.Down) && position. Y < 720)
                    {
                        position.Y += ShipSpeed * dt;
                    }

                    if (kState.IsKeyDown(Keys.Up) && position.Y > 0)
                    {
                        position.Y -= ShipSpeed * dt;
                    }
                }
        }
    }
}
