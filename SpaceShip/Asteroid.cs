﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpaceShip
{
    class Asteroid
    {
        private Vector2 position;
        private int speed;
        private int radius = 59;
        private bool offScreen = false;

        static Random randYValue = new Random();

        public Asteroid(int newSpeed)
        {
            speed = newSpeed;
            
            position = new Vector2(1280 + radius, randYValue.Next(59,721));
            
        }

        public int Speed { get => speed; set => speed = value; }
        public int Radius { get => radius; set => radius = value; }
        public Vector2 Position { get => position; set => position = value; }
        public bool OffScreen { get => offScreen; set => offScreen = value; }

        public void asteroidUpdate (GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;
            position.X -= speed * dt;
        }
    }
}
